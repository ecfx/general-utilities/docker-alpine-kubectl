FROM alpine:latest
RUN apk add --no-cache curl ca-certificates
RUN curl https://storage.googleapis.com/kubernetes-release/release/v{version}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
RUN echo "{sha256sum}  /usr/local/bin/kubectl" | sha256sum -c
RUN set -x && \
    chmod +x /usr/local/bin/kubectl && \
    \
    # Create non-root user (with a randomly chosen UID/GUI).
    adduser kubectl -DS && \
    \
    # Basic check it works.
    kubectl version --client

USER kubectl