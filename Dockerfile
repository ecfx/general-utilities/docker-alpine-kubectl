FROM alpine:latest
RUN apk add --no-cache curl ca-certificates jq
RUN curl https://storage.googleapis.com/kubernetes-release/release/v1.14.2/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl
RUN echo "9daf696f5609ab41ac491886de5e87e35d8d3076f65defc27602a476800a97bf  /usr/local/bin/kubectl" | sha256sum -c
RUN set -x && \
    chmod +x /usr/local/bin/kubectl && \
    \
    # Create non-root user (with a randomly chosen UID/GUI).
    adduser kubectl -DS && \
    \
    # Basic check it works.
    kubectl version --client

USER kubectl
