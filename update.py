#!/usr/bin/env python

import requests
import srcinfo.parse

url = 'https://aur.archlinux.org/cgit/aur.git/plain/.SRCINFO?h=kubectl-bin'
response = requests.get(url)
response.raise_for_status()
raw_srcinfo = response.text

parsed_srcinfo = srcinfo.parse.parse_srcinfo(raw_srcinfo)[0]

version = parsed_srcinfo['pkgver']
sha256sum = parsed_srcinfo['sha256sums_x86_64'][0]


with open('Dockerfile.tpl') as tpl_fh:
    dockerfile_tpl = tpl_fh.read()

dockerfile_contents = dockerfile_tpl.format(version=version, sha256sum=sha256sum)
print(dockerfile_contents)